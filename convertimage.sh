#!/bin/bash

#temp script: TBD better.

function usage {
 echo "`basename $0` image"
}

[ -z $1 ] && usage && exit 1
IMGFILE=$1
IMGFILEOUT=${IMGFILE/qcow2/raw}
qemu-img convert -f qcow2 -O raw $IMGFILE $IMGFILEOUT 
# Note - if this step is ommited, VMs spawned from this image will not
# configure ipv4 correctly. Reference: INC2246166
echo "Configuring cloud-init to not generate network configuration"
LIBGUESTFS_BACKEND=direct guestfish -a $IMGFILEOUT -i <<_EOF_
        touch /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg
        write /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg "network: {config: disabled}"
_EOF_
