#!/bin/bash
# The following script runs CERN CentOS functional tests

yum install git cern-linuxsupport-access -y

cern-linuxsupport-access enable
git clone https://gitlab.cern.ch/linuxsupport/testing/cern_centos_functional_tests.git
cd cern_centos_functional_tests;

./runtests.sh