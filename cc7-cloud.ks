#version=RHEL7
#perform install
install
# System authorization information
auth --enableshadow --passalgo=sha512
# Use text mode install
text
# Do not configure the X Window System
skipx
# Firewall configuration AFS and ARCD
firewall --enabled --service=ssh --port=7001:udp --port=4241:tcp
# No firstboot
firstboot --disable
# Network information
network  --bootproto=dhcp
# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'
# System language
lang en_US.UTF-8
# Reboot after installation
rootpw --iscrypted [this-is-not-a-root-password]
# SELinux configuration
selinux --enforcing
# System services
services --disabled="kdump,rhsmcertd,wpa_supplicant" --enabled="network,sshd,rsyslog"
# License agreement
eula --agreed
# System timezone
timezone --utc Europe/Zurich
# this should not be neeed
ignoredisk --only-use=vda
# System bootloader configuration
bootloader --append="console=ttyS0,115200 console=tty0 inst.gpt" --location=mbr --timeout=5 --boot-drive=vda
# Clear the Master Boot Record
zerombr
# Partition clearing information
clearpart --all --disklabel gpt
# Auto partitioning depending on firmware
reqpart
# Disk partitioning information
part / --fstype="xfs" --ondisk=vda --size=3000 --mkfsoptions="-n ftype=1"


# overlay for XFS/docker: https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/7/html/7.2_Release_Notes/technology-preview-file_systems.html
# Reboot after installation
reboot

repo --name="CentOS-7 - Base" --baseurl http://linuxsoft.cern.ch/cern/centos/7.7/os/$basearch/
# The following repo lines are commented as our koji tag builds already define these as external repos
#repo order is *important* for koji image build target !
#repo --name="CentOS-7 - CERN" --baseurl http://linuxsoft.cern.ch/cern/centos/7/cern/$basearch/
#repo --name="CentOS-7 - Updates" --baseurl http://linuxsoft.cern.ch/cern/centos/7/updates/$basearch/
#repo --name="CentOS-7 - Extras" --baseurl http://linuxsoft.cern.ch/cern/centos/7/extras/$basearch/

%packages
@cern-base
@cern-openafs-client
chrony
dracut-config-generic
dracut-norescue
firewalld
grub2
iptables-services
kernel
krb5-workstation
redhat-lsb-core
nfs-utils
ntp
rsync
tar
vim-enhanced
yum-utils
yum-plugin-ovl
mdadm
-aic94xx-firmware
-alsa-firmware
-alsa-lib
-alsa-tools-firmware
-biosdevname
-lcm-firstboot
-iprutils
-ivtv-firmware
-iwl1000-firmware
-iwl100-firmware
-iwl105-firmware
-iwl135-firmware
-iwl2000-firmware
-iwl2030-firmware
-iwl3160-firmware
-iwl3945-firmware
-iwl4965-firmware
-iwl5000-firmware
-iwl5150-firmware
-iwl6000-firmware
-iwl6000g2a-firmware
-iwl6000g2b-firmware
-iwl6050-firmware
-iwl7260-firmware
-iwl7265-firmware
-libertas-sd8686-firmware
-libertas-sd8787-firmware
-libertas-usb8388-firmware
-linux-firmware
-mcelog
-plymouth
-xorg-x11-font-utils
cloud-init
cloud-utils-growpart
cern-config-users
cern-private-cloud-addons
NetworkManager-DUID-LLT
mesa-libGL
locmap
nscd
sendmail-cf
gdisk
%end

# not taken into account by image factory ...
%addon cern_customizations  --run-locmap --afs-client --auto-update
%end

%post --log=/root/anaconda-post.log

# lock root account
passwd -d root
passwd -l root

yum -y update
#yum -y install cloud-init cloud-utils-growpart cern-config-users cern-private-cloud-addons NetworkManager-DUID-LLT mesa-libGL locmap
#rpm -e kernel-3.10.0-514.el7 linux-firmware
rpm -e linux-firmware

# cannot login on the console anyway ...
sed -i 's/^#NAutoVTs=.*/NAutoVTs=0/' /etc/systemd/logind.conf
# installation time shutdown problem ?
systemctl restart systemd-logind

# disable cloud-init managing network (new in 7.4)
cat >> /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg << EOF
network: {config: disabled}
EOF

# make the system reask for ip .. just in case
cat >> /etc/sysconfig/network-scripts/ifcfg-eth0 << EOF
DHCPV6C=yes
PERSISTENT_DHCLIENT=1
NOZEROCONF=1
EOF

sed -i 's|^HWADDR=.*||' /etc/sysconfig/network-scripts/ifcfg-eth0
rm -f /etc/udev/rules.d/70-persistent-net.rules


# set virtual-guest as default profile for tuned
tuned-adm profile virtual-guest

# fix rtc to use utc not local time (INC0974179)
# done via --utc timzezone switch
#timedatectl set-local-rtc 0
#cat >> /etc/adjtime << EOF
#0.0 0 0.0
#0
#UTC
#EOF

# and identical chrony.keys (INC0980266)
echo "" > /etc/chrony.keys

#no tmpfs for /tmp."
systemctl mask tmp.mount

# root - enabled, cloud user - disabled.
if [ -e /etc/cloud/cloud.cfg ]; then
    sed -i 's|^disable_root: 1|disable_root: 0|' /etc/cloud/cloud.cfg
    sed -i 's|\- default||' /etc/cloud/cloud.cfg
    sed -i 's|^users:||' /etc/cloud/cloud.cfg

    # fix cloud-init (INC0974035 RH 01594925)
    sed -i -e 's/\&\s\~/\& stop/' /etc/rsyslog.d/21-cloudinit.conf
    # and fix it again !
    sed -i -e 's/\[CLOUDINIT\]/cloud-init/' /etc/rsyslog.d/21-cloudinit.conf
    sed -i -e 's/syslogtag/programname/' /etc/rsyslog.d/21-cloudinit.conf
    sed -i -e 's/cloud-init.log/cloud-init-output.log/' /etc/rsyslog.d/21-cloudinit.conf
fi

# default rpm keys imported.
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-cern
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-kojiv2
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-CentOS-7
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7

# enable locmap default modules
locmap --enable afs
locmap --enable kerberos
locmap --enable lpadmin
locmap --enable nscd
locmap --enable ntp
locmap --enable sendmail
locmap --enable ssh
locmap --enable sudo


# clean up installation
rm -f /tmp/{ks-script-*,yum.log}
rm -f /root/anaconda-{ks,pre,post}.log
rm -f /root/{anaconda,original}-ks.cfg
rm -rf /var/cache/yum/*

# Ironic fix for RAID1 at boot time (Disabled for now)
/usr/bin/dracut -v --add 'mdraid' --add-drivers 'raid1 raid10 raid5 raid0 linear' --regenerate-all --force

# Ironic fix rd.auto
sed -i -e 's/crashkernel/rd.auto net.ifnames=0 crashkernel/g' /etc/default/grub

# Depending on UEFI or BIOS, ref. https://access.redhat.com/solutions/3081331
if [ -d /sys/firmware/efi ] ; then
/usr/sbin/grub2-mkconfig -o /boot/efi/EFI/centos/grub.cfg
else
/usr/sbin/grub2-mkconfig -o /boot/grub2/grub.cfg
fi

# stamp the build
/bin/date "+%Y%m%d %H:%M" > /etc/.BUILDTIME
%end

