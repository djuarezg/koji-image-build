#version=RHEL7
# Use text mode install
text
# Do not configure the X Window System
skipx
# Firewall configuration for SSH
firewall --enabled --service=ssh
# No firstboot
firstboot --disable
# Network information
network  --bootproto=dhcp
# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'
# System language
lang en_US.UTF-8
# Reboot after installation
rootpw --iscrypted [this-is-not-a-root-password]
# SELinux configuration
selinux --enforcing
# System services
services --disabled="kdump" --enabled="sshd"
# License agreement
eula --agreed
# System timezone
timezone --utc Europe/Zurich
# this should not be neeed
ignoredisk --only-use=vda
# System bootloader configuration
bootloader --append="console=ttyS0,115200 console=tty0 inst.gpt" --location=mbr --timeout=5 --boot-drive=vda
# Clear the Master Boot Record
zerombr
# Partition clearing information
clearpart --all --disklabel gpt
# Disk partitioning information
part / --fstype="xfs" --ondisk=vda --size=3000 --mkfsoptions="-n ftype=1"
part biosboot --fstype=biosboot --size=1

reboot

%packages
@^minimal-environment
-biosdevname
-iprutils
-iwl1000-firmware
-iwl100-firmware
-iwl105-firmware
-iwl135-firmware
-iwl2000-firmware
-iwl2030-firmware
-iwl3160-firmware
-iwl3945-firmware
-iwl4965-firmware
-iwl5000-firmware
-iwl5150-firmware
-iwl6000-firmware
-iwl6000g2a-firmware
-iwl6050-firmware
-iwl7260-firmware
CERN-CA-certs
epel-release
krb5-workstation
nfs-utils
redhat-lsb-core
rsync
tar
vim-enhanced
yum-utils
%end

%post --log=/root/anaconda-post.log

yum -y install cloud-init cloud-utils-growpart cern-get-keytab

# lock root account
passwd -d root
passwd -l root

rpm -e linux-firmware

# cannot login on the console anyway ...
sed -i 's/^#NAutoVTs=.*/NAutoVTs=0/' /etc/systemd/logind.conf
# installation time shutdown problem ?
systemctl restart systemd-logind

# disable cloud-init managing network (new in 7.4)
cat >> /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg << EOF
network: {config: disabled}
EOF

# make the system reask for ip .. just in case
cat >> /etc/sysconfig/network-scripts/ifcfg-eth0 << EOF
DHCPV6C=yes
PERSISTENT_DHCLIENT=1
NOZEROCONF=1
EOF

sed -i 's|^HWADDR=.*||' /etc/sysconfig/network-scripts/ifcfg-eth0
rm -f /etc/udev/rules.d/70-persistent-net.rules


# set virtual-guest as default profile for tuned
tuned-adm profile virtual-guest

# and identical chrony.keys (INC0980266)
echo "" > /etc/chrony.keys

#no tmpfs for /tmp."
systemctl mask tmp.mount

# root - enabled, cloud user - disabled.
if [ -e /etc/cloud/cloud.cfg ]; then
    sed -i 's|^disable_root: 1|disable_root: 0|' /etc/cloud/cloud.cfg
    sed -i 's|\- default||' /etc/cloud/cloud.cfg
    sed -i 's|^users:||' /etc/cloud/cloud.cfg

    # fix cloud-init (INC0974035 RH 01594925)
    sed -i -e 's/\&\s\~/\& stop/' /etc/rsyslog.d/21-cloudinit.conf
fi

# default rpm keys imported.
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-cern
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-kojiv2
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-centosofficial
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-8

# clean up installation
rm -f /tmp/{ks-script-*,yum.log}
rm -f /root/anaconda-{ks,pre,post}.log
rm -f /root/{anaconda,original}-ks.cfg
rm -rf /var/cache/dnf/*

# Ironic fix for RAID1 at boot time (Disabled for now)
/usr/bin/dracut -v --add 'mdraid' --add-drivers 'raid1 raid10 raid5 raid0 linear' --regenerate-all --force

# Ironic fix rd.auto
sed -i -e 's/crashkernel=auto/rd.auto net.ifnames=0 crashkernel=auto/g' /etc/default/grub

/usr/sbin/grub2-mkconfig -o /boot/grub2/grub.cfg
/usr/sbin/grub2-mkconfig -o /boot/efi/EFI/centos/grub.cfg

# stamp the build
/bin/date "+%Y%m%d %H:%M" > /etc/.BUILDTIME
%end
